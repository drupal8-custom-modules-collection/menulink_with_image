# Add Media image field to MenuLinkContent

A media image field inside the MenuLinkContent to extend the menu functionality.

## Depencencies

Drupal core 8.4+

## Installation

Download and enable the module inside you modules directory and run update.php
